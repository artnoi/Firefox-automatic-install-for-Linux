#!/bin/sh
#
# Firefox automatic install for Linux
#   v2.9.2.0.0
#
while true :
do
 clear;
 printf -- '\n%s\n' " ";
 printf -- '%s\n' "   6 4 b i t - C H O I C E" \
 " " \
 " 1. System install for everyone (requires admin access)." \
 " 2. Personal install for only yourself." \
 " 3. Exit" \
 "" ""
 printf " Please enter option [1 - 3]";
 read -r opt
 case $opt in
  1) clear; printf -- '\n%s\n\n' " You selected system install for everyone"; 
     chmod +x ./64bit/sub_menu64.sh; ./64bit/sub_menu64.sh; exit 0 ;;

  2) clear; printf -- '\n%s\n\n' " You selected personal install for only yourself"; 
     chmod +x ./personal/sub_personal.sh; ./personal/sub_personal.sh; exit 0 ;;

  3) clear; printf -- '\n%s\n\n' " Goodbye, $USER"; exit 1;;

  *) clear;
     printf -- '\n\n%s\n' " $opt is an invaild option. Please select option between 1-3 only" \
     " Press the [enter] key to continue. . ."
     read -r enterKey;
     clear;
esac
done
