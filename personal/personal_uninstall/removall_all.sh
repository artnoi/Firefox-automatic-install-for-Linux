#!/bin/sh
#
# Firefox automatic install for Linux - personal uninstall ALL
# v2.9.1.0.0
#
############ Mozilla Firefox
chmod +x ./unFirefox.sh ; bash ./unFirefox.sh ;
#
############ Firefox Beta
chmod +x ./unBeta.sh ; bash ./unBeta.sh ;
#
############ Firefox Developer Edition
chmod +x ./unDeveloper.sh ; bash ./unDeveloper.sh ;
#
############ Firefox Nightly
chmod +x ./unNightly.sh ; bash ./unNightly.sh ;
#
############ Firefox Extended Support Release
chmod +x ./unExtended.sh ; bash ./unExtended.sh ;
#
# Exit
exit 0
