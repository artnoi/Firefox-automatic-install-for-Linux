#!/bin/sh
#
# Firefox automatic install for Linux - personal uninstall ALL
# v2.9.1.0.0
#
# -- This file to be used with Setup.sh
#
############ Mozilla Firefox
chmod +x ./personal/personal_uninstall/unFirefox.sh ; bash ./personal/personal_uninstall/unFirefox.sh ;
#
############ Firefox Beta
chmod +x ./personal/personal_uninstall/unBeta.sh ; bash ./personal/personal_uninstall/unBeta.sh ;
#
############ Firefox Developer Edition
chmod +x ./personal/personal_uninstall/unDeveloper.sh ; bash ./personal/personal_uninstall/unDeveloper.sh ;
#
############ Firefox Nightly
chmod +x ./personal/personal_uninstall/unNightly.sh ; bash ./personal/personal_uninstall/unNightly.sh ;
#
############ Firefox Extended Support Release
chmod +x ./personal/personal_uninstall/unExtended.sh ; bash ./personal/personal_uninstall/unExtended.sh ;
#
# Exit
exit 0
