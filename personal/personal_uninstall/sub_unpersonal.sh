#!/bin/sh
#
# Firefox automatic install for Linux
#   v2.9.1.0.0
#
# -- This file to be used with Setup.sh
#
# That which is done, cannot be undone. Reinstalled, of course! But not undone.
while true :
do
printf -- '\n%s\n' " ";
printf -- '%s\n' "   U N I N S T A L L - P E R S O N A L - M E N U" \
" " \
" CAUTION - You are about to remove and delete" \
"           Mozilla Firefox from your computer!" \
" " \
" 1. Mozill Firefox" \
" 2. Firefox Beta" \
" 3. Firefox Developer Edition" \
" 4. Firefox Nightly" \
" 5. Firefox Extended Support Release" \
" 6. Uninstall ALL personal 64-bit editions" \
" 7. Exit" \
"" ""
 printf " Please enter option [1 - 7]";
 read -r opt
 case $opt in
  1) clear; printf -- '\n%s\n\n' " You selected Mozilla Firefox"; 
     chmod +x ./personal/personal_uninstall/unFirefox.sh; ./personal/personal_uninstall/unFirefox.sh; break ;;

  2) clear; printf -- '\n%s\n\n' " You selected Firefox Beta"; 
     chmod +x ./personal/personal_uninstall/unBeta.sh; ./personal/personal_uninstall/unBeta.sh; break ;;

  3) clear; printf -- '\n%s\n\n' " You selected Firefox Developer Edition"; 
     chmod +x ./personal/personal_uninstall/unDeveloper.sh; ./personal/personal_uninstall/unDeveloper.sh; break ;;

  4) clear; printf -- '\n%s\n\n' " You selected Firefox Nightly"; 
     chmod +x ./personal/personal_uninstall/unNightly.sh; ./personal/personal_uninstall/unNightly.sh; break ;;

  5) clear; printf -- '\n%s\n\n' " You selected Firefox Extended Support Release"; 
     chmod +x ./personal/personal_uninstall/unExtended.sh; ./personal/personal_uninstall/unExtended.sh; break ;;

  6) clear; printf -- '\n%s\n\n' " You selected to remove ALL 64-bit editions"; 
     chmod +x ./personal/personal_uninstall/sub_unall.sh; ./personal/personal_uninstall/sub_unall.sh; break ;;

  7) clear; printf -- '\n%s\n\n' " Goodbye, $USER"; exit 1;;
  

  *) clear;
     printf -- '\n\n%s\n' " $opt is an invaild option. Please select option between 1-7 only" \
     " Press the [enter] key to continue. . ."
     read -r enterKey;
     clear;
esac
done
