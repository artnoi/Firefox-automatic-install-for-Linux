#!/bin/sh
#
# This will install ALL 64-bit releases.

# Firefox automatic install for Linux
# v2.9.2.0.0
#
printf -- '\n%s\n' " Now installing ALL editions of Mozilla Firefox. Please wait... ";
# Give time for user to read notice.
sleep 2; 
# Visual spacing
printf -- '\n\n\n%s\n\n' " ";
# Firefox Stable
chmod +x ./Firefox_Stable.sh; ./Firefox_Stable.sh;
# Firefox Beta
chmod +x ./Firefox_Beta.sh; ./Firefox_Beta.sh;
# Firefox Developer Edition
chmod +x ./Firefox_Developer_Edition.sh; ./Firefox_Developer_Edition.sh;
# Firefox Nightly
chmod +x ./Firefox_Nightly.sh; ./Firefox_Nightly.sh;
# Firefox Extended Support Release
chmod +x ./Firefox_ESR.sh; ./Firefox_ESR.sh;
# Exit notice
printf -- '%s\n' "" "" "" " Congratulations!" \
  " ALL editions of Mozilla Firfox have been installed onto your computer." \
  " They ALL will update themselves. No additional action is required." \
  " Happy browsing." "" ""
# Exit
exit 0